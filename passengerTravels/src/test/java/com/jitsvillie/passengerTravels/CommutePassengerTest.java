package com.jitsvillie.passengerTravels;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jitsvillie.PassengerFactory.PassengerFactory;
import com.jitsvillie.passenger.CommuterPassenger;
import com.jitsvillie.passenger.IPassenger;
import com.jitsvillie.passenger.PassengerPOJO;

public class CommutePassengerTest {
	IPassenger passenger;
	PassengerPOJO passengerPOJO;
	PassengerFactory pf = new PassengerFactory();

	
	@Before
	public void SetUp() {
		pf = new PassengerFactory();
		
		passenger = pf.getPassenger("COMMUTERPASSENGER");
	}

	// Test the value of rateFactor
	@Test

	public void rateFactorTest() {

		assertEquals(0.5, IPassenger.RATE_FACTOR, 0.001);
	}

	@Test
//test the commuter for discount of  frequentRider
	public void commuterCostDiscountTest() {
		
		passengerPOJO = new PassengerPOJO("Shru", 3, 100, true, true, true);

		double actual= passenger.passengerFareCost(passengerPOJO);
		double expected = 1.35;
		assertEquals(expected, actual, 0.001);
		
		
	}
	
	@Test
	//test the commuter for discount of  frequentRider
		public void commuterCostTest() {
			
			passengerPOJO = new PassengerPOJO("Shru", 3, 100, false, true, true);

			double actual= passenger.passengerFareCost(passengerPOJO);
			double expected = 1.5;
			assertEquals(expected, actual, 0.001);
			
			
		}
}
