package com.jitsvillie.passengerTravels;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.jitsvillie.journey.Journey;
import com.jitsvillie.passenger.PassengerPOJO;

public class JourneyTest {
	List<PassengerPOJO> pg;
	Journey journey;

	@Before

	public void doSetUp() {
		journey = new Journey();

		pg = new ArrayList<PassengerPOJO>();

		pg.add(new PassengerPOJO("Shru", 3, 100, true, false, false));
		pg.add(new PassengerPOJO("Sharu", 5, 100, true, false, false));
		pg.add(new PassengerPOJO("dudu", 4, 100, false, false, true));

		pg.add(new PassengerPOJO("bibu", 2, 90, false, true, false));
		pg.add(new PassengerPOJO("pistachio", 3, 199, false, true, true));
	}

	@Test
	public void totalNoOfPapersTest() {

		assertEquals(2, journey.totalNoOfPapers(pg), 0.001);
	}

	@Test
	public void totalNoOfMealsTest() {
		assertEquals(2, journey.totalNoOfMeals(pg), 0.001);
	}

	@Test
	public void costOfAllPassengers() {

		assertEquals(150.5, journey.costOfAllPassengers(pg), 001);

	}

}
