package com.jitsvillie.passengerTravels;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jitsvillie.Meal.IMeal;
import com.jitsvillie.PassengerFactory.PassengerFactory;
import com.jitsvillie.passenger.CommuterPassenger;
import com.jitsvillie.passenger.IPassenger;
import com.jitsvillie.passenger.PassengerPOJO;
import com.jitsvillie.passenger.VaccationPassenger;

public class VaccationPassengerTest {
	IPassenger passenger;
	PassengerPOJO passengerPOJO;
	IMeal meal;
	PassengerFactory pf = new PassengerFactory();

	@Before
	public void SetUp() {

		pf = new PassengerFactory();
		passenger= pf.getPassenger("VACCATIONPASSENGER");
		meal=(IMeal) pf.getPassenger("VACCATIONPASSENGER");
	}

	// Test the value of rateFactor

	@Test
	// test the commuter for discount of frequentRider
	public void vaccationerFareTest() {

		passengerPOJO = new PassengerPOJO("Shru", 3, 100, false, true, true);

		double actual = passenger.passengerFareCost(passengerPOJO);
		double expected = 50;
		assertEquals(expected, actual, 0.001);

	}

	@Rule
	public ExpectedException anException = ExpectedException.none();

	@Test
	// test the commuter for discount of frequentRider
	public void vaccationerFareRangeTest() {

		anException.expect(IllegalArgumentException.class);
		anException.expectMessage("No of miles are out of ranger for vaccationer");
		passengerPOJO = new PassengerPOJO("Shru", 3, 5000, false, true, true);

		double actual = passenger.passengerFareCost(passengerPOJO);

	}

	@Test
	// test the commuter for discount of frequentRider
	public void vaccationerMealTest() {

		

		int actual = meal.mealCalculation(new PassengerPOJO("Shru", 3, 100, false, true, true));
		double expected = 1;
		assertEquals(expected, actual, 0.001);

	}
	

}
