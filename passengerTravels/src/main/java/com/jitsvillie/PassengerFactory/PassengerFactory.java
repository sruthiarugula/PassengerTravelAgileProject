package com.jitsvillie.PassengerFactory;

import com.jitsvillie.passenger.CommuterPassenger;
import com.jitsvillie.passenger.IPassenger;
//import com.jitsvillie.passenger.PassengerPOJO;
import com.jitsvillie.passenger.VaccationPassenger;

public class PassengerFactory {

	public IPassenger getPassenger(String passengerType) {
		IPassenger passenger =null;

		if ("VACCATIONPASSENGER".equalsIgnoreCase(passengerType)) {
			passenger = new VaccationPassenger();

		} else if (("COMMUTERPASSENGER".equalsIgnoreCase(passengerType))) {
			passenger = new CommuterPassenger();
		}
		return passenger;
	}

}
