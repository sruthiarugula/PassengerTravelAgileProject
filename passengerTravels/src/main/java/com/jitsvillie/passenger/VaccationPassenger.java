package com.jitsvillie.passenger;

import com.jitsvillie.Meal.IMeal;

public class VaccationPassenger implements IPassenger, IMeal {
	
	
	

	public double passengerFareCost(PassengerPOJO p) {

		double vaccationerCost = 0;

		// TODO Auto-generated method stub

		if (p.getNoOfMiles() < 4 || p.getNoOfMiles() > 4000) {

			throw new IllegalArgumentException("No of miles are out of ranger for vaccationer");
		} else {
			vaccationerCost = RATE_FACTOR * p.getNoOfMiles();
		}
		return vaccationerCost;

	}

	public int mealCalculation(PassengerPOJO pg) {
		// TODO Auto-generated method stub
		int noOfMeals = 0;

		if (pg.mealRequired) {
			noOfMeals = pg.getNoOfMiles() / 100;
			if ((pg.getNoOfMiles() % 100) < 100 && ((pg.getNoOfMiles() % 100>0))) {
				noOfMeals++;
			}
		}
		return noOfMeals;
	}

}
