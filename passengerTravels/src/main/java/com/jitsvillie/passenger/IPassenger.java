package com.jitsvillie.passenger;

public interface IPassenger {
	
	//variable Rate factor is a constant ,initialised in the interface
	double RATE_FACTOR=0.5;
	
double passengerFareCost(PassengerPOJO p);

}
