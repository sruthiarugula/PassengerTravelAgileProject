package com.jitsvillie.passenger;

public class PassengerPOJO {
	 
	 String passengerName;
	 int noOfStops;
	 int noOfMiles;
	 boolean frequentRiderCard;
	 boolean mealRequired;
	 boolean newsPaperRequired;
	public boolean isNewsPaperRequired() {
		return newsPaperRequired;
	}
	public void setNewsPaperRequired(boolean newsPaperRequired) {
		this.newsPaperRequired = newsPaperRequired;
	}
	public String getPassengerName() {
		return passengerName;
	}
	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}
	public int getNoOfStops() {
		return noOfStops;
	}
	public void setNoOfStops(int noOfStops) {
		this.noOfStops = noOfStops;
	}
	public int getNoOfMiles() {
		return noOfMiles;
	}
	public void setNoOfMiles(int noOfMiles) {
		this.noOfMiles = noOfMiles;
	}
	public boolean isFrequentRiderCard() {
		return frequentRiderCard;
	}
	public void setFrequentRiderCard(boolean frequentRiderCard) {
		this.frequentRiderCard = frequentRiderCard;
	}
	public boolean isMealRequired() {
		return mealRequired;
	}
	public void setMealRequired(boolean mealRequired) {
		this.mealRequired = mealRequired;
	}
	
	public PassengerPOJO(String passengerName, int noOfStops, int noOfMiles, boolean frequentRiderCard,
			boolean mealRequired, boolean newsPaper) {
		super();
		this.passengerName = passengerName;
		this.noOfStops = noOfStops;
		this.noOfMiles = noOfMiles;
		this.frequentRiderCard = frequentRiderCard;
		this.mealRequired = mealRequired;
		this.newsPaperRequired=newsPaper;
	}
	 

}
