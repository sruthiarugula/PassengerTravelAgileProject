package com.jitsvillie.Meal;

import com.jitsvillie.passenger.PassengerPOJO;

public interface IMeal  {
	
	int mealCalculation(PassengerPOJO pg);

}
