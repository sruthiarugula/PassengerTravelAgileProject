package com.jitsvillie.journey;

import java.util.List;

import com.jitsvillie.PassengerFactory.PassengerFactory;
import com.jitsvillie.passenger.IPassenger;
import com.jitsvillie.passenger.PassengerPOJO;

public class Journey {

	public int totalNoOfPapers(List<PassengerPOJO> listPassengerPOJO) {
		int NoOfPapers = 0;

		for (int i = 0; i < listPassengerPOJO.size(); i++) {
			if (listPassengerPOJO.get(i).isNewsPaperRequired()) {
				NoOfPapers++;
			}

		}

		return NoOfPapers;

	}

	public int totalNoOfMeals(List<PassengerPOJO> listPassengerPOJO) {

		int NoOfMeals = 0;
		for (int i = 0; i < listPassengerPOJO.size(); i++) {
			if (listPassengerPOJO.get(i).isMealRequired()) {
				NoOfMeals++;
			}

		}
		return NoOfMeals;
	}

	public double costOfAllPassengers(List<PassengerPOJO> listPassengerPOJO) {
		double costOfAllPassengers = 0;
		PassengerFactory pf = new PassengerFactory();
	

		IPassenger ip ;

		for (int i = 0; i < listPassengerPOJO.size(); i++) {
			
			if(!(listPassengerPOJO.get(i).isFrequentRiderCard()) && (listPassengerPOJO.get(i).isMealRequired()) ){
				ip=pf.getPassenger("VACCATIONPASSENGER");
				costOfAllPassengers += ip.passengerFareCost(listPassengerPOJO.get(i));
			}
			else{
				ip=pf.getPassenger("COMMUTERPASSENGER");
				costOfAllPassengers += ip.passengerFareCost(listPassengerPOJO.get(i));
			}
			

		}

		return costOfAllPassengers;

	}

}
